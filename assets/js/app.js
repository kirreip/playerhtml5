var Player = (function () {
  function Player () {
  }

  Player.prototype.playFunc = () => {
    if (Player.prototype.player.paused && !Player.prototype.player.ended) {
      Player.prototype.player.play()
      Player.prototype.playingFunc()
    } else {
      Player.prototype.player.pause()
      Player.prototype.playInput.innerHTML = 'Play'
    }
  }
  Player.prototype.muteFunc = () => {
    if (Player.prototype.player.volume > 0) {
      lastStateVolume = Player.prototype.player.volume
      Player.prototype.volumeBar.value = 0
      Player.prototype.player.volume = 0
    } else {
      Player.prototype.player.volume = (lastStateVolume > 0) ? lastStateVolume : 1
      Player.prototype.volumeBar.value = Player.prototype.player.volume * 100
    }
  }
  Player.prototype.volumeDownFunc = () => {
    inter = setInterval(() => {
      Player.prototype.player.volume = Player.prototype.volumeBar.value / 100
    }, 50)
  }
  Player.prototype.volumeUpFunc = () => {
    clearInterval(inter)
  }
  Player.prototype.progressChangeFunc = () => {
    Player.prototype.player.currentTime = (Player.prototype.player.duration / 100) * Player.prototype.progressBar.value
  }
  Player.prototype.playerUpdateFunc = () => {
    Player.prototype.progressBar.value = (Player.prototype.player.currentTime / Player.prototype.player.duration) * 100
  }
  Player.prototype.updateUpFunc = () => {
    Player.prototype.player.play()
  }
  Player.prototype.updateDownFunc = () => {
    Player.prototype.player.pause()
  }
  Player.prototype.waitingFunc = () => {
    Player.prototype.playInput.innerHTML = 'Loading'
  }
  Player.prototype.playingFunc = () => {
    Player.prototype.playInput.innerHTML = 'Pause'
  }
  Player.prototype.endedFunc = () => {
    Player.prototype.playFunc()
    Player.prototype.player.currentTime = 0
  }
  Player.prototype.fullScreenFunc = () => {
    if (Player.prototype.player.requestFullscreen) {
      Player.prototype.player.requestFullscreen()
    } else if (Player.prototype.player.webkitRequestFullscreen) {
      Player.prototype.player.webkitRequestFullscreen()
    }
  }
  Player.prototype.player = document.getElementById('player')
  Player.prototype.playInput = document.getElementById('play')
  Player.prototype.muteInput = document.getElementById('mute')
  Player.prototype.fullScreenInput = document.getElementById('fullScreen')
  Player.prototype.progressBar = document.getElementById('progress')
  Player.prototype.volumeBar = document.getElementById('volume')
  var inter // Saved Interval to clear it after mouseup
  var lastStateVolume = 0 // to save the last volume before mute
  return Player
})()

window.onload = () => {
  window.player = new Player() // to get the player overall
  window.player.player.controls = false // remove controls

  // Play Button
  window.player.playInput.addEventListener('click', window.player.playFunc)

  // Mute Button
  window.player.muteInput.addEventListener('click', window.player.muteFunc)

  // volumeBar clickdown and move the mouse
  window.player.volumeBar.addEventListener('mousedown', window.player.volumeDownFunc)

  // just to clear inter on mouseup
  window.player.volumeBar.addEventListener('mouseup', window.player.volumeUpFunc)

  // handle moving on progressBar
  window.player.progressBar.addEventListener('change', window.player.progressChangeFunc)
  window.player.player.addEventListener('timeupdate', window.player.playerUpdateFunc)

  // to avoid some bug while the mouse is moving on the progressBar
  window.player.progressBar.addEventListener('mousedown', window.player.updateDownFunc)
  window.player.progressBar.addEventListener('mouseup', window.player.updateUpFunc)

  // Loading
  window.player.player.addEventListener('waiting', window.player.waitingFunc)
  window.player.player.addEventListener('playing', window.player.playingFunc)

  // back to the beginning after the end
  window.player.player.addEventListener('ended', window.player.endedFunc)

  // toggleFullScreen
  window.player.fullScreenInput.addEventListener('click', window.player.fullScreenFunc)

  window.player.player.addEventListener('click', window.player.playFunc)
}
