const path = require('path')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const WebpackAssetsManifest = require('webpack-assets-manifest')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin');

const dev = process.env.NODE_ENV === "dev"

const ExtractTextPluginLoader = ExtractTextPlugin.extract({
	fallback: "style-loader",
	use: [{
			loader: 'css-loader',
			options: {
				importLoaders: 1,
				minimize: !dev
			}
		},
		{
			loader: 'postcss-loader',
			options: {
				plugins: (loader) => [
					require('autoprefixer')({
						browsers: ['last 2 versions', 'ie > 8']
					})
				]
			}
		},
		'sass-loader'
	]
})


let config = {
	entry: {
		player: ['./assets/css/player.scss', './assets/js/app.js'],
	},
	output: {
		path: path.resolve('./dist/assets/'),
		filename: dev ? '[name].js' : '[name].[chunkhash:8].js',
		publicPath: '/assets/'
	},
	watch: dev,
	devtool: dev ? 'cheap-module-eval-source-map' : false,
	devServer: {
		contentBase: path.resolve('./public')
	},
	module: {
		rules: [{
				test: /\.(png|jpg|gif)$/,
				use: [{
					loader: 'url-loader',
					options: {
						limit: 8192,
						name: '[name].[hash:8].[ext]'
					}
				}, {
					loader: 'img-loader',
					options: {
						enabled: !dev
					}
				}]
			},
			{
				enforce: 'pre',
				test: /\.js$/,
				exclude: /(node_modules|bower_components)/,
				use: [{
					loader: 'eslint-loader',
					options: {
						fix: true,
						emitWarning: true,
						emitError: false,
					}
				}],
			}, {
				test: /\.js$/,
				exclude: /(node_modules|bower_components)/,
				use: ['babel-loader']
			},
			{
				test: /\.css$/,
				use: ExtractTextPluginLoader
			},
			{
				test: /\.scss$/,
				use: ExtractTextPluginLoader
			}
		],
	},
	plugins: [
		new ExtractTextPlugin({
			filename: dev ? '[name].css' : '[name].[contenthash:8].css',
			disable: false
		}),
		new HtmlWebpackPlugin(),
		new CleanWebpackPlugin([
			'dist',
		], {
			root: path.resolve('./'),
			verbose: true,
			dry: false
		})
	],
}

if (!dev) {
	config.plugins.push(new UglifyJsPlugin({
		sourceMap: false
	}))
	config.plugins.push(new WebpackAssetsManifest({
		// Options go here 
	}))
}

module.exports = config