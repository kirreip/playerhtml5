# Player HTML5

Small player for HTML5 <video>

## Getting Started

### Prerequisites

```
git clone https://kirreip@bitbucket.org/kirreip/playerhtml5.git
cd playerhtml5
```

### Installing

```
npm i
```

## Running the tests

All tests are in the Browser direcotry:

Just open index.html with your browser

##Dev

you can run a local sever on (http://localhost:8080/) with:
```
npm run wd
```
(wd means Webpack-Dev)

## Deployment

you can generate all files with:
```
npm run wp
```
(wp means Webpack-Production)

## Built With

* [Webpack](https://webpack.js.org/) - to dev
* [Mocha](https://mochajs.org/) - Tests
* [ChaiJs](http://chaijs.com/) - Tests
* [SinonJS](http://sinonjs.org/) - Tests
* [BigBuckBunny](https://peach.blender.org/) - Movie

