var expect = chai.expect
var player = window.app

before((done) => {
	setTimeout(() => {
		done()
	}, 500)
}) // to wait the loading else in some cases all tests fail after the first loading

describe('Player', () => {

	describe('PlayFunc for the Play Button', () => {

		it('Should have playInput', () => {
			expect(player).to.have.property('playFunc')
		})

		it('Should use player.play() First', () => {
			sinon.spy(player.player, 'play')
			player.playFunc()
			expect(player.player.play.calledOnce).to.be.true
			player.player.play.restore()
		})

		it('Should use player.pause() Second', () => {
			sinon.spy(player.player, 'pause')
			player.playFunc()
			expect(player.player.pause.calledOnce).to.be.true
			player.player.pause.restore()
		})

		it('Should play First', () => {
			player.playFunc()
			expect(player.player.paused).to.be.false
		})

		it('Should stop Second', () => {
			player.playFunc()
			expect(player.player.paused).to.be.true
		})
	})


	describe('ProgressBar', () => {
		it('Should have player.progressChangeFunc', () => {
			expect(player).to.have.property('progressChangeFunc')
		})
		it('Should have player.playerUpdateFunc', () => {
			expect(player).to.have.property('playerUpdateFunc')
		})
		it('Should have updateUpFunc', () => {
			expect(player).to.have.property('updateUpFunc')
		})
		it('Should have player.updateDownFunc', () => {
			expect(player).to.have.property('updateDownFunc')
		})
		it('Should use player.play() Up', () => {
			sinon.spy(player.player, 'play')
			player.updateUpFunc()
			expect(player.player.play.calledOnce).to.be.true
			player.player.play.restore()
		})
		it('Should use player.pause() Down', () => {
			sinon.spy(player.player, 'pause')
			player.updateDownFunc()
			expect(player.player.pause.calledOnce).to.be.true
			player.player.pause.restore()
		})
	})
})